# -*- coding: utf-8 -*-
"""
Created on Fri May 22 18:41:23 2020

@author: junik
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import colour
import itertools
#tabulated csv file to read. See documentation.
filename = 'data/spectral_sample.txt'

#reading spectral data
data = pd.read_csv(filename, delimiter = '\t', header = 0)
header = list(data.columns)[1:]
npdata = data.to_numpy()
wavelength = npdata[1:,0]
spectral_data =  npdata[1:,1:].transpose()
led_number = np.shape(spectral_data)[0] 

#generating SpectralDistribution objects from data
spectra = []
i=0
for p_distribution in spectral_data:
    SD = colour.SpectralDistribution(p_distribution, wavelength)
    SD.name = header[i]
    i += 1
    spectra.append(SD)

#calculating CIE 1931 tristimulus values
XYZ_coordinates =[]
for lightsource in spectra:
    XYZ_coordinates.append(colour.sd_to_XYZ(lightsource))
XYZ_coordinates = np.array(XYZ_coordinates) 
   
colour.plotting.plot_multi_sds(spectra)
colour.plotting.diagrams.plot_sds_in_chromaticity_diagram(spectra)

#calculating CIE1931 xyY coordinates for 3D gamut 
#only a few GS levels are used for the generation of gamut points.
xy_coors= []
for key in list(itertools.product([0, 0.1, 0.5, 0.9,  1], repeat=led_number)):
    xy_coors.append(colour.XYZ_to_xyY((np.transpose(XYZ_coordinates.T * key)).sum(axis=0)))

xy_coors = np.array(xy_coors)
x = xy_coors[:, 0]    
y = xy_coors[:, 1]  
z = xy_coors[:, 2]  

#plotting 3D gamut with the calculated points
fig= plt.figure()
ax=plt.axes(projection='3d')
luminance_map = ax.scatter3D(x, y, z, c=z, cmap='inferno');
ax.set_xlabel('x')
ax.set_ylabel('y')
cbar = fig.colorbar(luminance_map)
cbar.ax.set_ylabel('luminance in [cd/m^2]')
ax.view_init(90, -90)
plt.show()


    