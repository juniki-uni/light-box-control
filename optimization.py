# -*- coding: utf-8 -*-
"""
Created on Fri May 22 22:51:28 2020

@author: junik
"""

import numpy as np
import colour
from scipy.optimize import least_squares
from scipy.optimize import differential_evolution
import time
import copy

#basically this is the error function
def difference(sd, xyY, target_sd, target_xyY, color_importance=100, normalize_sd_lum = True):
    #color importance weights color error (opposed to spectal error)
    #normalize luminance of spectrum
    if (normalize_sd_lum):
        target_sd *= (colour.luminous_flux(sd) / colour.luminous_flux(target_sd))
        
    Lab1 = colour.XYZ_to_Lab(colour.xyY_to_XYZ(xyY))
    Lab2 = colour.XYZ_to_Lab(colour.xyY_to_XYZ(target_xyY))
        
    #according to CIE 1976 method
    color_error = colour.delta_E(Lab1, Lab2) * color_importance
    
    spectral_error = np.sqrt(((sd.values - target_sd.values)**2).sum())

    return color_error + spectral_error

#wrapper DE and least squares algorithm cost function
def cost(x, spectra, target_spectrum, target_xyY):
#    x: array_like coefficients for each specta
    
    #calculate mixed color from weights (x) and specra
    w_spectra = []
    for i in range(len(spectra)):
        w_spectra.append(spectra[i] * x[i])
        
    mixed_sd = copy.copy(w_spectra[0])
    for i in range(1, len(spectra)):
        mixed_sd += w_spectra[i]
      
    mixed_xyY = colour.XYZ_to_xyY(colour.sd_to_XYZ(mixed_sd))

    return difference(mixed_sd, mixed_xyY, target_spectrum, target_xyY)

def find_coefficients(spectra, target_xyY, target_sd = colour.sd_constant(1e-2)):
    start = time.time()
    print('target xyY:', target_xyY)
    
    for sd in spectra:
        sd.align(colour.SpectralShape(380, 780, 5))
        sd.align(colour.SpectralShape(380, 780, 5))
        sd.align(colour.SpectralShape(380, 780, 5))
        #bugfix :(
    target_sd.align(colour.SpectralShape(380, 780, 5))
    
    minmax = [(0, 1) for i in range(len(spectra))]
    lower_bound = np.zeros((len(spectra)))
    upper_bound = np.ones((len(spectra)))
    
    #using DE to loosly find global minimum of cost function
    optimum = differential_evolution(cost, 
                                     minmax, 
                                     args = (spectra, target_sd, target_xyY), 
                                     tol = 0.1,
                                     popsize = 3,
                                     mutation = (0.2, 0.01),
                                     polish = False
                                     )
    #calculating result with the found parameters
    w_spectra = []
    for i in range(len(spectra)):
        w_spectra.append(spectra[i] * optimum.x[i])
        
    mixed_sd = colour.sd_zeros(shape=colour.SpectralShape(380, 780, 5))
    mixed_sd.name = 'mixed spectrum'
    for i in range(0, len(spectra)):
        mixed_sd += w_spectra[i]
    w_spectra.append(mixed_sd)
    
    achieved_xyY = colour.XYZ_to_xyY(colour.sd_to_XYZ(mixed_sd))   
    print('After Diff_Evolution (xyY):', achieved_xyY)
    
    #using DE parameters as a start 
    x0 = optimum.x
    #refining parameters locally, using differential method
    optimum = least_squares(cost, 
                            x0, 
                            args = (spectra, target_sd, target_xyY),
                            bounds = (lower_bound, upper_bound), 
                            diff_step=0.005,
                            ftol = 0.01
                            )
    #calculating result with the found parameters again
    w_spectra = []
    for i in range(len(spectra)):
        w_spectra.append(spectra[i] * optimum.x[i])
        
    mixed_sd = colour.sd_zeros(shape=colour.SpectralShape(380, 780, 5))
    mixed_sd.name = 'mixed spectrum'
    for i in range(0, len(spectra)):
        mixed_sd += w_spectra[i]
    w_spectra.append(mixed_sd)
    
    achieved_xyY = colour.XYZ_to_xyY(colour.sd_to_XYZ(mixed_sd))   
    print('After refining (xyY):', achieved_xyY)
    print(time.time()-start, 's elapsed time')
    
    return optimum, mixed_sd, w_spectra

if (__name__ == '__main__'):
    #spectra from spectrumControl.py
    optimum, achieved_sd, weighted_spectra = find_coefficients(spectra, 
                                                               [0.2, 0.6, 5], 
                                                               colour.sd_CIE_standard_illuminant_A())